

#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */

#include "helper.h"           /*  Our own helper functions  */
#include "helper.c"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <netdb.h>


/*  Global constants  */

#define BUFLEN  (2048)
#define SRV_IP "127.0.0.1"

/*  Function declarations  */

int ParseCmdLine(int argc, char *argv[], char **szAddress, char **szPort);


/*  main()  */

int main(int argc, char *argv[]) {
    struct hostent *hp;
    //int       conn_s;                /*  connection socket         */
   // short int tcp_port;
    short int port;                  /*  port number               */
  //  struct    sockaddr_in servaddr;  /*  socket address structure  */
    struct sockaddr_in si_other;
    struct sockaddr_in remaddr;
    char      buf[BUFLEN];      /*  character buffer          */
    char     *szAddress;             /*  Holds remote IP address   */
    char     *szPort;                /*  Holds remote port         */
    char     *endptr;  
    int fd;              /*  for strtol()              */
    int slen = sizeof(si_other);
    socklen_t addrlen = sizeof(remaddr);  
    int recvlen;          /* length of addresses */

    /*  Get command line arguments  */

    ParseCmdLine(argc, argv, &szAddress, &szPort);


    /*  Set the remote port  */

    port = strtol(szPort, &endptr, 0);
    if ( *endptr ) {
	printf("ECHOCLNT: Invalid port supplied.\n");
	exit(EXIT_FAILURE);
    }
	
    /*create socket*/
    if ( (fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
    fprintf(stderr, "ECHOCLNT: Error creating socket.\n");
    exit(EXIT_FAILURE);
    }
    /*  Create the listening socket  

    if ( (conn_s = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
	fprintf(stderr, "ECHOCLNT: Error creating listening socket.\n");
	exit(EXIT_FAILURE);
    }
    */

    /*  Set all bytes in socket address structure to
        zero, and fill in the relevant data members   */

    memset((char *)&si_other, 0, sizeof(si_other));
    si_other.sin_family      = AF_INET;
    si_other.sin_port        = htons(port);

//    hp = gethostbyaddr(&szAddress, sizeof(szAddress), AF_INET);    
  //  printf("Host name: %s\n", hp->h_name);

//    memcpy((void *)&si_other.sin_addr, hp->h_addr_list[0], hp->h_length);
    /*  Set the remote IP address  */

    if ( inet_aton(SRV_IP, &si_other.sin_addr) == 0 ) {
	printf("ECHOCLNT: Invalid remote IP address.\n");
	exit(EXIT_FAILURE);
    }

    
    /*  connect() to the remote echo server  */
/*
    if ( connect(conn_s, (struct sockaddr *) &servaddr, sizeof(servaddr) ) < 0 ) {
	printf("ECHOCLNT: Error calling connect()\n");
	exit(EXIT_FAILURE);
    }
*/

    /*  Get string to echo from user  */

    printf("Send to server: ");
    char name_of_file[32];
    // /*taking in input and storing into char*/
    char first_inp;
    char sec_inp;
    
    do {
         first_inp = getchar();
         sec_inp = getchar();    
     } while((first_inp != 's') && (first_inp != 't') && (first_inp != 'q'));    //checking initial input
    
    if (first_inp == 'q'){
         return 0;
     }else if(first_inp == 's'){
         char c;
         int i = 0;
    //     /*taking characters one by one to make sure \n is included at the end of the string*/
         while ((c = getchar()) != '\n'){
             buf[i++] = c;
         }
         buf[i] = '\0';

    // /*concatenation of CAP\\n with string input and load into buffer*/
         char dest[32], src[12];
         strcpy(src, "\\n");
         strcpy(dest, "CAP\\n");
         strcat(dest, buf);
         strcat(dest, src);
         strcpy(buf, dest);
         buf[strlen(buf)] = '\n';
}
    // }else if(first_inp == 't'){
    //    char c;
    //     int i = 0;
    //     /*taking characters one by one to make sure \n is included at the end of the string*/
    //     while ((c = getchar()) != '\n'){
    //         buffer[i++] = c;
    //     }
    //     buffer[i] = '\0';
        
    //     strcpy(name_of_file, buffer);
    // /*concatenation of FILE\\n with string input and load into buffer*/
    //     char dest[32], src[12];
    //     strcpy(src, "\\n");
    //     strcpy(dest, "FILE\\n");
    //     strcat(dest, name_of_file);
    //     //printf("%s\n", dest);
    //     strcat(dest, src);
    //     strcpy(buffer, dest);
    //     buffer[strlen(buffer)] = '\n';
    //     //printf("%s\n", buffer);
        

    // }
   

    /*  Send string to server, and retrieve response  */

    for (int i =0; i < 10; i++){
        printf("Sending packet %d\n", i);
        if (sendto(fd, buf, BUFLEN, 0, (struct sockaddr *)&si_other, slen)== -1){
            printf("ECHOCLNT: Could not send.\n");
            exit(EXIT_FAILURE);
        }
}
    recvlen = recvfrom(fd, buf, BUFLEN, 0, (struct sockaddr *)&remaddr, &addrlen);
    //printf("received %d bytes\n", recvlen);
    if (recvlen > 0){
        buf[recvlen] = 0;
        printf("received message: \"%s\"\n", buf);
    }
    close(fd);
    return 0;
}



//     Writeline(conn_s, buffer, strlen(buffer));
//     Readline(conn_s, buffer, MAX_LINE-1);
//     /*writing the file to server directory */
//     if(first_inp == 't'){
//         char size_of_file[64];
//         char b;
//         int i = 0;
//         while ((buffer[i]) != '\n'){    /* \n separates size of file and the content of file*/
//             size_of_file[i++] = b;
//         }
//         size_of_file[i] = '\0';
//         FILE *writefile;
//         slice out the content of the file so that it can be written
//         for (int j = 0; j < strlen(buffer) - strlen(size_of_file)-2; j++){
//             buffer[i] = buffer[strlen(size_of_file)+2+i];
//         }
//         buffer[strlen(buffer)] = '\0';

//         writefile = fopen(name_of_file, "wb");
//         if (writefile){
//             fwrite(buffer, sizeof(buffer), 1, writefile);
//         }}
        
//       /*Output echoed string  */

//     printf("Echo response: %s\n", buffer);

//     return EXIT_SUCCESS;


/*function to avoid using -a and -p*/
int ParseCmdLine(int argc, char *argv[],  char **szAddress, char **szPort) {

	*szAddress = argv[1];
	*szPort = argv[2];

    return 0;
}
