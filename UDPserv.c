

#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */

#include "helper.h"           /*  our own helper functions  */
#include "helper.c"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>


/*  Global constants  */

#define ECHO_PORT  (2002)
#define BUFSIZE  (2048)

long GetFileSize(const char*);

int main(int argc, char *argv[]) {
    //int       list_s;                /*  listening socket          */
    //int       conn_s;                /*  connection socket         */
    short int port;                  /*  port number               */
    struct    sockaddr_in servaddr;  /*  socket address structure  */
    struct sockaddr_in remaddr;		/*remote address*/
    socklen_t addrlen = sizeof(remaddr);
    int recvlen;					/*bytes received*/
    int fd;							/* our socket */
    char buf[BUFSIZE];		/*receive buffer*/
    //char      buffer[MAX_LINE + 2];      /*  character buffer          */
    char     *endptr;                /*  for strtol()              */


    /*  Get port number from the command line, and
        set to default port if no arguments were supplied  */

    if ( argc == 2 ) {
	port = strtol(argv[1], &endptr, 0);
	if ( *endptr ) {
	    fprintf(stderr, "ECHOSERV: Invalid port number.\n");
	    exit(EXIT_FAILURE);
	}
    }
    else if ( argc < 2 ) {
	port = ECHO_PORT;
    }
    else {
	fprintf(stderr, "ECHOSERV: Invalid arguments.\n");
	exit(EXIT_FAILURE);
    }

	
    /*  Create the listening socket  */

    if ( (fd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
	fprintf(stderr, "ECHOSERV: Error creating listening socket.\n");
	exit(EXIT_FAILURE);
    }

    memset((char *)&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(port);


    /*  Bind our socket addresss to the 
	listening socket, and call listen()  */

    if ( bind(fd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 ) {
	fprintf(stderr, "ECHOSERV: Error calling bind()\n");
	exit(EXIT_FAILURE);
    }
/*
    if ( listen(list_s, LISTENQ) < 0 ) {
	fprintf(stderr, "ECHOSERV: Error calling listen()\n");
	exit(EXIT_FAILURE);
    }*/

    
    /*  Enter an infinite loop to respond
        to client requests and echo input  */

    while ( 1 ) {

	/*  Wait for a connection, then accept() it  */
    //printf("waiting on port %d\n", port);
    recvlen = recvfrom(fd, buf, BUFSIZE, 0, (struct sockaddr *)&remaddr, &addrlen);
	//printf("received %d bytes\n", recvlen);
	if (recvlen > 0){
		buf[recvlen] = 0;
		printf("received message: \"%s\"\n", buf);
	}
}
	if (sendto(fd, buf, BUFSIZE, 0, (struct sockaddr *)&remaddr, addrlen)== -1){
            printf("ECHOSERV: Could not send.\n");
            exit(EXIT_FAILURE);
        }
	
close(fd);
}

//never exits
	/*
	if ( (conn_s = accept(list_s, NULL, NULL) ) < 0 ) {
	    fprintf(stderr, "ECHOSERV: Error calling accept()\n");
	    exit(EXIT_FAILURE);
	}
	*/

    
	/*  Retrieve an input line from the connected socket
	    then simply write it back to the same socket.     */
/*
	Readline(conn_s, buffer, MAX_LINE-1);
	
	if ((buffer[0] == 'C') && (buffer[1] == 'A') && (buffer[2] == 'P')){
		int i = 0;
		int j = 0;
		int count = strlen(buffer) - 8;
*/
	/* loop to slice out the CAP/n characters, and store the capitalized string into buffer*/
/*		for (int i = 0; i < count; i++){
		
			if (buffer[i] == '\n') {
				buffer[i] = '\0';
				break;
			}
			buffer[j] = toupper(buffer[5 + j]);
			j += 1;
		}
		buffer[j] = '\0';
*/
	/*concatenation to add the no.of characters in string, /n, and the capitalized string*/
/*		
		char fwd_slh[12];
		char cnt[64];
		char temp[1024];
		strcpy(fwd_slh, "\\n");
		sprintf(cnt, "%d", count);
		sprintf(temp, "%s%s", cnt, fwd_slh);
		strcat(temp, buffer);
		strcpy(buffer, temp);

	}else if ((buffer[0] == 'F') && (buffer[1] == 'I') && (buffer[2] == 'L') && (buffer[3] == 'E')){
		*//*tasks within FILE\nxxx\n */
		/*int i = 0;
		int j = 0;
		int count = strlen(buffer) - 9;*/
	/* loop to slice out the FILE/n characters, and store the  string into buffer*/
	/*	for (int i = 0; i < count; i++){
			if (buffer[i] == '\n') {
				buffer[i] = '\0';
				break;
			}
			buffer[j] = buffer[6 + j];
			j += 1;
		}
		buffer[j] = '\0';*/
		/*check to see if the file exists, and read the file, putting contents into a char array*/
		/*
		if(access(buffer, F_OK) != -1){
			FILE *fp;
			char ch;
			char filetext[104856];	//setting the limit of content filetext can handle
			fp = fopen(buffer, "rb");
			int i = 0;
			while((ch = fgetc(fp)) != EOF){
				
				filetext[i] = ch;	//read characters in the file and load onto filetext char array
				i++;
			}
			filetext[strlen(filetext)] = '\0';
			fclose(fp);	*/
			//file exists
			
		/*	long filesize;
			filesize = GetFileSize(buffer);	//calculate size of the file
			
			char fwd_slh[12];
			char size[64];
			char temp[1024];
			

			strcpy(fwd_slh, "\\n");	
			sprintf(size, "%ld", filesize);
			sprintf(temp, "%s%s", size, fwd_slh);
			strcat(temp, filetext);
			strcpy(buffer, temp);
			//buffer[strlen(buffer)] = '\n';


		
		}else{
			strcpy(buffer, "Not Found");
			//file does not exists
		}
		
	//}
	
	

	Writeline(conn_s, buffer, strlen(buffer));


	 Close the connected socket 

	if ( close(conn_s) < 0 ) {
	    fprintf(stderr, "ECHOSERV: Error calling close()\n");
	    exit(EXIT_FAILURE);
	}

   } 
}
*/

/*
long GetFileSize(const char* filename){
	long size;
	FILE *f;
	f = fopen(filename, "rb");
	if (f == NULL){
		return -1;
	}
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fclose(f);
	return size;
}*/



